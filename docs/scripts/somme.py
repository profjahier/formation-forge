# --------- PYODIDE:code --------- #

def somme_par_indices_pairs(nombres):
    somme = ...
    for ... in ...:
        if ...:
            somme = ...

    return ...

def somme_des_valeurs_paires(nombres):
    somme = ...
    for ... in ...:
        if ...:
            somme = ...

    return ...

# --------- PYODIDE:corr --------- #

def somme_par_indices_pairs(nombres):
    somme = 0
    for i in range(0, len(nombres), 2):
        somme += nombres[i]
    return somme

def somme_des_valeurs_paires(nombres):
    somme = 0
    for n in nombres:
        if n % 2 == 0:
            somme = somme + n
    return somme


# --------- PYODIDE:tests --------- #

assert somme_par_indices_pairs([]) == 0
assert somme_des_valeurs_paires([]) == 0
assert somme_par_indices_pairs([4, 6, 3])== 7
assert somme_des_valeurs_paires([4, 6, 3])== 10


# --------- PYODIDE:secrets --------- #

assert somme_des_valeurs_paires([4, 6, 8])== 18
assert somme_par_indices_pairs([6, 8])== 6