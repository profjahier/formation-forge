---
author: Ronan Jahier
title: Exercice n°1
---

# Bienvenue

## Exo 1

### Indices ou valeurs ? ([Codex](https://codex.forge.apps.education.fr/exercices/indices_valeurs/){:target="_blank"})

On donne un tableau de nombres nombres.

Compléter le code des deux fonctions ci-dessous :

- somme_par_indices_pairs prend le tableau en argument et renvoie la somme des valeurs placées à des indices pairs ;

- somme_des_valeurs_paires prend le tableau en argument et renvoie la somme des valeurs paires.

*Par exemple :*

{{ IDE('scripts/somme', MAX=42, SANS='sum, max') }}




